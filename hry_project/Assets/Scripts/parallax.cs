﻿using System;
using UnityEngine;

namespace lucidGame
{
    public class Parallax : MonoBehaviour
    {

    
        /* OTHER */
        [SerializeField] private Transform m_Camera;
        private Vector3 m_LastCameraPosition;
        private float m_Length;
        private Vector3 m_StartPos;

        [SerializeField] private float parallaxEffect;
        // Start is called before the first frame update
        void Start()
        {
            m_StartPos = transform.position;
            m_Length = GetComponent<SpriteRenderer>().bounds.size.x;
            m_LastCameraPosition = m_Camera.position;
        }

        
        void LateUpdate()
        {
            float temp = (m_Camera.position.x * (1 - parallaxEffect));
            Vector3 deltaMovement = m_Camera.position - m_LastCameraPosition;
            float distance = (deltaMovement.x * parallaxEffect);

            transform.position = new Vector3(m_StartPos.x + distance, transform.position.y, transform.position.z);

            
            if (temp > m_StartPos.x + m_Length) m_StartPos.x += m_Length;
            else if (temp < m_StartPos.x - m_Length) m_StartPos.x -= m_Length;
    
            m_LastCameraPosition = m_Camera.position;

        }
        /*
        private void LateUpdate()
        {
            Vector3 deltaMovement = m_Camera.position - m_LastCameraPosition;
            //Vector3 deltaMovement = m_StartPos;
            //deltaMovement.x = m_Camera.position.x - m_LastCameraPosition.x;
            //deltaMovement.y = m_StartPos.y;
            //deltaMovement.z = m_StartPos.z;
            
            transform.position += deltaMovement * parallaxEffect;
            
            
            m_LastCameraPosition = m_Camera.position;
            
            //m_LastCameraPosition.x = m_Camera.position.x;
            //m_LastCameraPosition.y = m_StartPos.y;
            //m_LastCameraPosition.z = m_StartPos.z;
            
        }
        */
        
    }
}
