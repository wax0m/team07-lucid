﻿using System;
using lucidGame.Control.Inventory;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace lucidGame
{
    public class DoorOpener : MonoBehaviour
    {
        [SerializeField] private SceneLoader m_SceneLoader;
        
        [SerializeField] private Item m_RequiredItem;

        [SerializeField] private bool m_ItemOwned;
    
        [SerializeField] private bool m_IsNearby;

        [SerializeField] private GameObject m_ClosedDoors;

        [SerializeField] private GameObject m_OpenedDoors;

        //[SerializeField] private GameObject m_InsideLight;

        //[SerializeField] private GameObject m_LightFlare;

        [SerializeField] private GameObject[] m_DoorLights;

        private Inventory m_PlayerInventory;

        void Awake()
        {
            m_PlayerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
            m_IsNearby = false;

            if (m_RequiredItem == null) m_ItemOwned = true;
            foreach (GameObject light in m_DoorLights)
            {
                light.SetActive(false);
            }

            m_OpenedDoors.SetActive(false);
            m_ClosedDoors.SetActive(true);
        }

        private void LateUpdate()
        {
            //Debug.Log("Needed: " + m_RequiredItem);
            //Debug.Log(m_ItemOwned);
            //FindInventoryItem();
        }

        private void Update()
        {
            //m_ItemOwned = CheckInventoryForItem();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                m_IsNearby = true;
                //FindInventoryItem();
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                m_IsNearby = false;
            }
        }

        private void OnMouseDown()
        {
            Debug.Log("MouseDown clicked");
            if (m_IsNearby && /*m_ItemOwned*/CheckInventoryForItem())
            {
                OpenDoors();
                Invoke("NextLevel", 0.2f);
                //m_SceneLoader.LoadNextScene();
            }
        }

        private void OpenDoors()
        {
            m_ClosedDoors.SetActive(false);
            m_OpenedDoors.SetActive(true);
            
            // Light up

            //m_InsideLight.SetActive(true);
            DoorLightUp();
        }

        private void DoorLightUp()
        {
            
            for (int i = 0; i < m_DoorLights.Length; i++)
            {
                m_DoorLights[i].SetActive(true);
            }
            
        }
    
        private void NextLevel(){
            //SceneManager.LoadScene("Level_1_HP");
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            m_SceneLoader.LoadNextScene();
        }
        
        
        private bool CheckInventoryForItem()
        {
            bool m_Return = false;
            if (m_RequiredItem != null)
            {
                Debug.Log("You need an item: " + m_RequiredItem);
                //Debug.Log(m_PlayerInventory.slots.Length);
                for (int i = 0; i < m_PlayerInventory.slots.Length; i++)
                {
                    if (m_PlayerInventory.slots[i].transform.childCount != 0)
                    {
                        DragDropItem currentItem = m_PlayerInventory.slots[i].transform.GetChild(0).gameObject
                            .GetComponent<DragDropItem>();
                        Debug.Log(m_PlayerInventory.slots[i].transform.childCount);
                        Debug.Log(m_PlayerInventory.slots[i].transform.GetChild(0).gameObject.GetComponent<DragDropItem>());
                        if (currentItem.thisItem == m_RequiredItem)
                        {
                            GameObject.Destroy(currentItem.gameObject);
                            m_PlayerInventory.isFull[i] = false;
                            Debug.Log("Item owned");
                            m_Return = true;
                            m_ItemOwned = true;
                            return true;
                            break;
                        }
                    }
                }
            }
            //else m_Return = true;
            return m_Return;
        }
    }
}
