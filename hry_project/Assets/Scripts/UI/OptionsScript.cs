﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;

namespace lucidGame
{
    public class OptionsScript : MonoBehaviour
    {
        public AudioMixer audioMixer;
        public TMP_Dropdown resolutionDropdown;
        private bool isFullscreen;
        Resolution[] resolutions;

        void Start(){
            isFullscreen = false;
            resolutions = Screen.resolutions;
            resolutionDropdown.ClearOptions();
            List<string> options = new List<string>();
            int currentIndex = 0; 
            for(int i = 0; i < resolutions.Length; i++){
                string option = resolutions[i].width + "x" + resolutions[i].height;
                options.Add(option);
                if(resolutions[i].width  == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height){
                    currentIndex = i;
                }
            }
            resolutionDropdown.AddOptions(options);
            resolutionDropdown.value = currentIndex;
            resolutionDropdown.RefreshShownValue();
        }

        void Update(){
            FixedUpdate();
        }

        private void FixedUpdate(){
            if(Input.GetButtonDown("Fullscreen")){
                isFullscreen = !isFullscreen;
                Debug.Log("fullscreen");
                Screen.fullScreen = isFullscreen;
            }
        }

        public void SetResolution(int resIndex){
            Resolution res = resolutions[resIndex];
            Screen.SetResolution(res.width, res.height, Screen.fullScreen);
        }

        public void SetVolume (float volume){
            audioMixer.SetFloat("volume", volume);
        }

        public void SetQuality(int key){
            Debug.Log("Quality");
            QualitySettings.SetQualityLevel(key);
        }
    }
}
