﻿using UnityEngine;

namespace lucidGame.Control.Inventory
{    
    [CreateAssetMenu(fileName = "New Item", menuName = "Item")]
    public class Item : ScriptableObject
    {
        public new string name;
        public string description;
        public int phase;

        public bool craftable;
        /*
        public string requiredItemName;
        public string newItemName;
        public string pickUpCondition;
        public string worldInteraction;
        */
    }
}
