﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace lucidGame.Control.Inventory
{
    public class ItemSlot : MonoBehaviour, IDropHandler
    {
        private GameObject m_Player;
        
        private Inventory m_Inventory;

        [SerializeField] private int inventoryIndex;
        //private int inventoryIndex;
        private DragDropItem m_DraggingItem;

        private DragDropItem m_CurrentItem;

        private CraftableItem m_CraftableItem;
        //private RectTransform rectTransform;
        private void Start()
        {
            m_Player = GameObject.FindGameObjectWithTag("Player");
            m_Inventory = m_Player.GetComponent<Inventory>();
            //m_Inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
            inventoryIndex = transform.GetSiblingIndex();
        }

        private void Update()
        {
            if (transform.childCount <= 0)
            {
                m_Inventory.isFull[inventoryIndex] = false;
            }
            else m_Inventory.isFull[inventoryIndex] = true;
        }
        public void OnDrop(PointerEventData eventData)
        {
            m_DraggingItem = eventData.pointerDrag.GetComponent<DragDropItem>();
            m_CraftableItem = m_DraggingItem.GetComponent<CraftableItem>();
            if (m_DraggingItem != null)
            {
                if (this.transform.childCount == 0)
                {
                    Debug.Log("OnDrop to " + gameObject.name);
                    Debug.Log("You dropped: " + m_CraftableItem.titleName + " on empty slot");
                    m_DraggingItem.parentToReturnTo = this.transform;
                }
                
                else
                {
                    /*
                    Debug.Log("OnDrop to " + gameObject.name);
                    DragDropItem currentItem = this.transform.GetChild(0).gameObject.GetComponent<DragDropItem>();
                    currentItem.parentToReturnTo = item.parentToReturnTo;
                    item.parentToReturnTo = this.transform;
                    */
                    m_CurrentItem = this.transform.GetChild(0).gameObject.GetComponent<DragDropItem>();
                    Debug.Log("Spot already taken by: " + m_CurrentItem.thisItem.name);
                    /*
                    if (m_CurrentItem.thisItem == m_DraggingItem.requiredItem.thisItem
                        ||
                        m_CurrentItem.requiredItem.thisItem == m_DraggingItem.thisItem)
                    {
                    */
                    if (m_CurrentItem.thisItem == m_DraggingItem.requiredItem
                        ||
                        m_CurrentItem.requiredItem == m_DraggingItem.thisItem)
                    {
                        m_Inventory.isFull[inventoryIndex] = false;
                        //m_Inventory.isFull[m_DraggingItem.transform.parent.GetComponent<ItemSlot>().inventoryIndex] = false;
                        Debug.Log("You combined: " + m_DraggingItem.thisItem.name + 
                                  " and " + m_CurrentItem.thisItem.name);
                        
                        //GameObject.Destroy(m_CurrentItem.gameObject);
                        Debug.Log("Crafted: " + m_CurrentItem.craftingItem.name);
                        //GameObject.Destroy(m_DraggingItem.gameObject);
                        CraftOnDrop();
                    }
                }
                
            }
        }


        public void CraftOnDrop()
        {
            Debug.Log("Destroy");
            //transform.gameObject.Destroy(m_CurrentItem));
            DragDropItem newItem;
            if (m_CurrentItem.currentPhase < m_DraggingItem.currentPhase) newItem = m_DraggingItem.GetComponent<DragDropItem>().craftingItem;
            else newItem = m_CurrentItem.GetComponent<DragDropItem>().craftingItem;
            GameObject.Destroy(m_CurrentItem.gameObject);
            GameObject.Destroy(m_DraggingItem.gameObject);
            for (int i = 0; i < m_Inventory.slots.Length; i++)
            {
                Debug.Log("Searching for empty inventory slot");
                if (!m_Inventory.isFull[i])
                {
                    m_Inventory.isFull[i] = true;
                    //DragDropItem newItem = m_DraggingItem.GetComponent<DragDropItem>().craftingItem;
                    Instantiate(newItem, m_Inventory.slots[i].transform, false);
                    //Destroy(gameObject);
                    break;
                }
            }
        }
        /*
        public void Use()
        {
            
            //foreach (Transform child in transform)
            //{
            //    GameObject.Destroy(child.gameObject);
            //}
            
            if (transform.childCount <= 0)
            {
                Debug.Log("There is nothing to use");
            }
            else if (transform.childCount == 1)
            {
                GameObject.Destroy(m_CurrentItem.gameObject);
            }
        }
        */
    }
}
