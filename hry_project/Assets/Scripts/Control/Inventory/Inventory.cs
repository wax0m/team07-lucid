﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace lucidGame.Control.Inventory
{
    public class Inventory : MonoBehaviour
    {

        //private int itemSlots = 0;
        [SerializeField] public GameObject inventoryBar;
        
        [SerializeField] public bool[] isFull;

        [SerializeField] public GameObject[] slots;

        

        private int m_InventorySlotsCount;

        void Awake()
        {
            inventoryBar = GameObject.FindWithTag("InventoryBar");
            
            m_InventorySlotsCount = inventoryBar.transform.childCount;
            isFull = new bool[m_InventorySlotsCount];
            slots = new GameObject[m_InventorySlotsCount];
            
            int count = 0;
            foreach(Transform i in inventoryBar.transform){
                slots[count] = i.gameObject;
                count++;
            }
            /*
            slots = new GameObject[m_InventorySlotsCount];
            slots = inventoryBar.GetComponentsInChildren<GameObject>();
            */
        }

        void Update()
        {
            //Debug.Log(inventoryBar);
            //Debug.Log(m_InventorySlotsCount);
        }

    }
}
