﻿using System;
using System.Collections;
using UnityEngine;

namespace lucidGame
{
    public class PlayerClimbController : MonoBehaviour
    {

        private PlayerController m_PlayerController;

        private Vector2 m_LedgeTopPos;
        private GameObject ledge;

        [SerializeField] private float m_ClimbingTimeLength;
        [SerializeField] private float m_HorizontalLedgeOffset;


        [SerializeField] private Transform topPosCheck;
        private Transform castPoint;
        private Transform frontReachCheck;
        [SerializeField] private float frontClimbReach;
        
        
    
        private void Start()
        {
            m_PlayerController = GetComponent<PlayerController>();
            castPoint = m_PlayerController.castPoint;
            frontReachCheck = m_PlayerController.frontReachCheck;
            //frontReach = m_PlayerController.frontReach;

        }
        public void CheckLedge()
        {
            RaycastHit2D  hit2D = Physics2D.Linecast(castPoint.position, frontReachCheck.position, m_PlayerController.groundLayers);
            Vector2 topPosition = new Vector2(topPosCheck.position.x + frontClimbReach, topPosCheck.position.y);
            Vector2 lookDown = new Vector2(topPosition.x, m_PlayerController.col.bounds.min.y);
            RaycastHit2D ledgeHit2D = Physics2D.Linecast(topPosition, lookDown, m_PlayerController.groundLayers);
            /*
            if (m_FacingRight)
            {
                //hit2D = Physics2D.Raycast( frontReachCheck.position, Vector2.right, frontReach);
                hit2D = Physics2D.Raycast(castPoint.position, frontReachCheck.position);
                Debug.DrawRay(frontReachCheck.position, Vector2.right, Color.red);
            }
            else
            {
                //hit2D = Physics2D.Raycast( frontReachCheck.position, Vector2.left, frontReach);
                hit2D = Physics2D.Raycast(castPoint.position, frontReachCheck.position);
                Debug.DrawRay(frontReachCheck.position, Vector2.left, Color.red);
            }
            */
           
            
            Debug.DrawLine(castPoint.position, frontReachCheck.position, Color.yellow);
            Debug.DrawLine(topPosition, lookDown, Color.green);
            if (ledgeHit2D && ledgeHit2D.collider.gameObject.CompareTag("Pushable"))
            {
                Debug.Log("Ledge top pos detected");
            }
            if (hit2D && hit2D.collider.gameObject.CompareTag("Pushable"))
            {
                ledge = hit2D.collider.gameObject;
                Debug.Log(ledge);
                m_PlayerController.canClimb = true;
                m_PlayerController.isReachingFront = true;
                m_PlayerController.canDrag = true;
                if (m_PlayerController.m_FacingRight)
                {
                    m_LedgeTopPos = new Vector2(transform.position.x + m_HorizontalLedgeOffset,
                        ledge.GetComponent<Collider2D>().bounds.max.y + m_PlayerController.col.bounds.extents.y - m_PlayerController.col.offset.y /* + 0.5fcol.offset.y*/);
                }
                else
                {
                    m_LedgeTopPos = new Vector2(transform.position.x - m_HorizontalLedgeOffset, 
                        ledge.GetComponent<Collider2D>().bounds.max.y + m_PlayerController.col.bounds.extents.y - m_PlayerController.col.offset.y/*+ 0.5fcol.offset.y*/);
                }

                Debug.DrawLine(castPoint.position, m_LedgeTopPos, Color.red);
                //Debug.Log("Ledge in front");
            }
            else
            {
                m_PlayerController.canClimb = false;
                m_PlayerController.isReachingFront = false;
                m_PlayerController.canDrag = false;
            }
        }

        public void LedgeClimb()
        {
            if (m_PlayerController.canClimb && Input.GetAxisRaw("Vertical") > 0 && !m_PlayerController.isDragging)
            {
                
                m_PlayerController.m_PlayerAnimator.Play("Player_ClimbUp");
                //AdjustPlayerPosition();
                //float length = m_PlayerAnimator.animation.clip.length;
                //float length = m_PlayerController.m_PlayerAnimator.GetCurrentAnimatorStateInfo(0).length;
                //StartCoroutine(ClimbingLedge(m_LedgeTopPos, length));
                //StartCoroutine(ClimbingLedge(m_LedgeTopPos, length - 0.4f));
                StartCoroutine(ClimbingLedge(m_LedgeTopPos, m_ClimbingTimeLength));
            }
        }

        private void AdjustPlayerPosition()
        {
            if (m_PlayerController.m_FacingRight)
            {
                transform.position =
                    new Vector2((ledge.GetComponent<Collider2D>().bounds.min.x + m_PlayerController.col.bounds.extents.x + m_PlayerController.col.offset.x),
                        transform.position.y);
            }
            else
            {
                transform.position =
                    new Vector2((ledge.GetComponent<Collider2D>().bounds.max.x - m_PlayerController.col.bounds.extents.x - m_PlayerController.col.offset.x),
                        transform.position.y);
            }
        }

        
        private IEnumerator ClimbingLedge(Vector2 topOfPlatform, float duration)
        {
            
            float time = 0;
            m_PlayerController.isClimbing = true;
            m_PlayerController.isFrozen = true;
            
            Vector2 startValue = transform.position;
            // Animate
            while (time < duration)
            {
                transform.position = Vector2.Lerp(startValue, topOfPlatform, time / duration);
                time += Time.deltaTime;
                yield return null;
            }
            // Stop animating
            m_PlayerController.isClimbing = false;
            m_PlayerController.isFrozen = false;
        }
        
    }
}
