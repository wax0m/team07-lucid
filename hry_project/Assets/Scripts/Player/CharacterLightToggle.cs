﻿using UnityEngine;

namespace lucidGame
{
    public class CharacterLightToggle : MonoBehaviour
    {
        [SerializeField] private PlayerController m_PlayerController;
        [SerializeField] private Animator m_LightAnimator;
        // Start is called before the first frame update

        void Awake()
        {
            //m_PlayerController = GetComponent<PlayerController>();
            //m_LightAnimator = GameObject.FindGameObjectWithTag("PlayerLight").GetComponent<Animator>();
            //m_LightAnimator = transform.GetComponentInChildren<Animator>();
        }
        void Start()
        {
            m_PlayerController = this.transform.GetComponentInParent<PlayerController>();
            m_LightAnimator = GetComponent<Animator>();
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        void FixedUpdate()
        {
            if (m_PlayerController.isLightOn)
            {
                m_LightAnimator.Play("LightUpAn");
            }
            else if (!m_PlayerController.isLightOn)
            {
                m_LightAnimator.Play("LightOffAn");
            }
        }
    }
}
