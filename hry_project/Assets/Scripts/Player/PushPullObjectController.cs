﻿using System;
using UnityEngine;

namespace lucidGame
{
    public class PushPullObjectController : MonoBehaviour
    {

        [SerializeField] private bool isBoat;
        private BoatController m_BoatController;
        private GameObject player;
        private bool isPlayerNearby = false;

        private Rigidbody2D rbPlayer;

        private Rigidbody2D rbBox;
        //private Rigidbody2D rbBox, rbPlayer;
        private float dragSpeed;
    
        private Vector2 movement;

        void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player");
            rbBox = GetComponent<Rigidbody2D>();
            rbPlayer = player.GetComponent<Rigidbody2D>();
            dragSpeed = player.GetComponent<PlayerController>().DragSpeed;
            if (isBoat)
            {
                m_BoatController = GetComponent<BoatController>();
            }
        }

        void Update()
        {
            FixedUpdate();
        }

        private void FixedUpdate()
        {
            //rbBox.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
            //rbBox.constraints = RigidbodyConstraints2D.FreezePositionX;
            if (!isBoat)
            {
                FreezeThisObject();
            }

            if (isBoat && !m_BoatController.isOnWater)
            {
                FreezeThisObject();
            }
            

            if(/*isPlayerNearby &&*/ player.GetComponent<PlayerController>().canDrag && Input.GetButton("Push-Pull") && !player.GetComponent<PlayerController>().isLightOn)
            {
                player.GetComponent<PlayerController>().isDragging = true;
                player.GetComponent<Animator>().SetBool("Grab", true);
                
                //rbBox.constraints = RigidbodyConstraints2D.None;
                //rbBox.constraints = RigidbodyConstraints2D.FreezeRotation;
                UnFreezeThisObject();
                player.GetComponent<PlayerController>().setMoveSpeed(dragSpeed);
                movement.x = Input.GetAxisRaw("Horizontal");
                rbBox.velocity = new Vector2(movement.x * dragSpeed, rbPlayer.velocity.normalized.y);
            }
            else
            {
                
                player.GetComponent<PlayerController>().isDragging = false;
                player.GetComponent<Animator>().SetBool("Grab", false);
                player.GetComponent<PlayerController>().setMoveSpeed(player.GetComponent<PlayerController>()
                    .m_DefaultMoveSpeed);
            }
            
            // ATTEMPT TO FREEZE ONLY WHEN PLAYER IS NEARBY
            /*
            if (!player.GetComponent<PlayerController>().isReachingFront && !player.GetComponent<PlayerController>().isClimbing)
            {
                rbBox.constraints = RigidbodyConstraints2D.None;
                rbBox.bodyType = RigidbodyType2D.Dynamic;

            }

            if (player.GetComponent<PlayerController>().isClimbing)
            {
                rbBox.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
                rbBox.velocity = Vector2.zero;
                rbBox.bodyType = RigidbodyType2D.Kinematic;
            }
            */
        }

        private void OnTriggerEnter2D(Collider2D other){
            if(other.CompareTag("Player")){
                isPlayerNearby = true;
            }
        }

        private void OnTriggerExit2D(Collider2D other){
            if(other.CompareTag("Player")){
                isPlayerNearby = false;
            }
        }

        public void FreezeThisObject()
        {
            rbBox.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
            //rbBox.constraints = RigidbodyConstraints2D.FreezePositionX;
        }

        public void UnFreezeThisObject()
        {
            rbBox.constraints = RigidbodyConstraints2D.None;
        }

    }
}
