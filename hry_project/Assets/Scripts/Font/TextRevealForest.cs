﻿using System.Collections;
using TMPro;
using UnityEngine;


namespace lucidGame
{
    public class TextRevealForest : MonoBehaviour
    {
        [SerializeField] private GameObject lightCollider;
        [SerializeField] private GameObject lightReveal;
        [SerializeField] private TextMeshPro m_TextA, m_TextB, m_TextC, m_TextD, m_TextE, m_TextBoat;

        [SerializeField] private float m_isNearA, m_isNearB, m_isNearC, m_isNearD, m_isNearE, m_isNearBoat;
        private float maxDist = 3.5f;

        public float fadeSpeed = 5.0f;
        private bool showHint;

        void Awake()
        {
            showHint = false;
            m_TextA.enabled = false;
            m_TextB.enabled = false;
            m_TextC.enabled = false;
            m_TextD.enabled = false;
            m_TextBoat.enabled = false;
        }

        // Start is called before the first frame update
        void Start()
        {
            lightCollider = GameObject.FindGameObjectWithTag("LightReveal");
            lightReveal = GameObject.FindGameObjectWithTag("Player");
        }

        // Update is called once per frame
        void Update()
        {
            checkShowHint();
        }

        private void checkShowHint()
        {
            if (lightCollider.GetComponent<LightReveal>().m_IsNearby && lightReveal.GetComponent<PlayerController>().isLightOn)
            {
                showHint = true;
            }
            else
            {
                showHint = false;
            }
           
            checkIsNear();
            
            if (showHint)
            {
                if (m_isNearA < maxDist)
                {
                    StartCoroutine(FadeInCoroutine(m_TextA, m_isNearA));
                }
                else
                {
                    m_TextA.enabled = false;
                }
                
                if (m_isNearB < maxDist)
                {
                    StartCoroutine(FadeInCoroutine(m_TextB, m_isNearB));
                }
                else
                {
                    m_TextB.enabled = false;
                }
                if (m_isNearC < maxDist)
                {
                    StartCoroutine(FadeInCoroutine(m_TextC, m_isNearC));
                }
                
                else
                {
                    m_TextC.enabled = false;
                }
                
                if (m_isNearD < maxDist)
                {
                    StartCoroutine(FadeInCoroutine(m_TextD, m_isNearD));
                }
                else
                {
                    m_TextD.enabled = false;
                }
                
                if (m_isNearE < maxDist)
                {
                    StartCoroutine(FadeInCoroutine(m_TextE, m_isNearD));
                }
                else
                {
                    m_TextE.enabled = false;
                }
                
                if (m_isNearBoat < maxDist)
                {
                    StartCoroutine(FadeInCoroutine(m_TextBoat, m_isNearBoat));
                }
                else
                {
                    m_TextBoat.enabled = false;
                }
            }
            else
            {
                m_TextA.enabled = false;
                m_TextB.enabled = false;
                m_TextC.enabled = false;
                m_TextD.enabled = false;
                m_TextE.enabled = false;
                m_TextBoat.enabled = false;
            }
        }
        private void checkIsNear()
       {
            m_isNearA = Vector3.Distance(m_TextA.transform.position, lightCollider.transform.position);
            m_isNearB = Vector3.Distance(m_TextB.transform.position, lightCollider.transform.position);
            m_isNearC = Vector3.Distance(m_TextC.transform.position, lightCollider.transform.position);
            m_isNearD = Vector3.Distance(m_TextD.transform.position, lightCollider.transform.position);
            m_isNearE = Vector3.Distance(m_TextE.transform.position, lightCollider.transform.position);
            m_isNearBoat = Vector3.Distance(m_TextBoat.transform.position, lightCollider.transform.position);
       }

        IEnumerator FadeInCoroutine(TextMeshPro text, float near)
        {
            float waitTime = 0;
            float fadeTime = 1;
            while (waitTime < 1)
            {
                text.enabled = true;
                text.fontMaterial.SetColor("_FaceColor", Color.Lerp(Color.clear, Color.white, waitTime));
                text.fontMaterial.SetColor("_OutlineColor", Color.Lerp(Color.clear, Color.white, waitTime));
                text.fontSharedMaterial.SetFloat("_GlowPower", waitTime);
                yield return null;
                waitTime += Time.deltaTime / fadeSpeed;
                
                if (!showHint)
                {
                    text.enabled = false;
                    yield break;
                }
            }
        }

        IEnumerator FadeOutCoroutine(TextMeshPro text)
        {
            float waitTime = 1;
            while (waitTime > 0)
            {
                text.fontMaterial.SetColor("_FaceColor", Color.Lerp(Color.clear, Color.white, waitTime));
                text.fontSharedMaterial.SetFloat("_GlowPower", waitTime);
                yield return null;
                waitTime -= Time.deltaTime / fadeSpeed;
                text.enabled = false;
            }
        }
    }
}
