﻿using System.Collections;
using TMPro;
using UnityEngine;


namespace lucidGame
{
    public class TextRevealAlej : MonoBehaviour
    {
        [SerializeField] private GameObject lightCollider; 
        [SerializeField] private GameObject player;
        [SerializeField] private GameObject item;
        [SerializeField] private TextMeshPro m_TextRange, m_TextFind;
        [SerializeField] private TextMeshPro m_TextClimb, m_TextPushPull, m_TextPickUp, m_TextLightUp;
        
        
        private bool showOnceBox, showOnceItem;
        [SerializeField] private float m_isNearRange, m_isNearFind, m_IsNearItem;
        private float maxDist = 4.0f;

        public float fadeSpeed = 2.0f;
        [SerializeField] private bool m_ShowHint;

        void Awake()
        {
            m_ShowHint = false;
            m_TextRange.enabled = false;
            m_TextFind.enabled = false;
            m_TextClimb.enabled = false;
            m_TextPushPull.enabled = false;
            m_TextPickUp.enabled = false;
            m_TextLightUp.enabled = false;
            showOnceBox = false;
            showOnceItem = false;
        }

        // Start is called before the first frame update
        void Start()
        {
            lightCollider = GameObject.FindGameObjectWithTag("LightReveal");
            player = GameObject.FindGameObjectWithTag("Player");

        }

        // Update is called once per frame
        void Update()
        {
            checkShowHintWhenLightOn();
            checkIsNear();
            checkShowHintWhenNearby();

        }

        private void checkShow()
        {
            if (lightCollider.GetComponent<LightReveal>().m_IsNearby && player.GetComponent<PlayerController>().isLightOn)
            {
                m_ShowHint = true;
            }
            else
            {
                m_ShowHint = false;
            }
            
        }

        private void checkShowHintWhenLightOn()
        {
            checkShow();
            
            if (m_ShowHint)
            {
                if (m_isNearRange < maxDist)
                {
                    StartCoroutine(FadeInCoroutine(m_TextRange));
                }
                else
                {
                    m_TextRange.enabled = false;
                }

                if (m_isNearFind < maxDist)
                {
                    StartCoroutine(FadeInCoroutine(m_TextFind));
                }
                else
                {
                    m_TextFind.enabled = false;
                }
            }
            else
            {
                m_TextRange.enabled = false;
                m_TextFind.enabled = false;
            }

        }

        private void checkShowHintWhenNearby()
        {
            if (m_IsNearItem < maxDist)
            {
                if (!showOnceItem)
                {
                    StartCoroutine(FadeInCoroutinePernament(m_TextPickUp));
                    StartCoroutine(FadeInCoroutinePernament(m_TextLightUp));
                    showOnceItem = true;
                    
                }
                
            }
            if (player.GetComponent<PlayerController>().canDrag)
            {
                if (!showOnceBox)
                { 
                    StartCoroutine(FadeInCoroutinePernament(m_TextPushPull));
                    StartCoroutine(FadeInCoroutinePernament(m_TextClimb));
                    showOnceBox = true;
                }
            }
        }

       private void checkIsNear()
       {
           m_isNearRange = Vector3.Distance(m_TextRange.transform.position, lightCollider.transform.position);
           m_isNearFind = Vector3.Distance(m_TextFind.transform.position, lightCollider.transform.position);
           if (item != null) m_IsNearItem = Vector3.Distance(item.transform.position, player.transform.position);
       }

        IEnumerator FadeInCoroutine(TextMeshPro text)
        {
            float waitTime = 0;
            while (waitTime < 1)
            {
                text.enabled = true;
                text.fontMaterial.SetColor("_FaceColor", Color.Lerp(Color.clear, Color.white, waitTime));
                text.fontMaterial.SetColor("_OutlineColor", Color.Lerp(Color.clear, Color.white, waitTime));
                text.fontSharedMaterial.SetFloat("_GlowPower", waitTime);
                yield return null;
                waitTime += Time.deltaTime / fadeSpeed;

               if (!m_ShowHint)
               {
                    text.enabled = false;
                    yield break;
               }
            }
        }
        
        IEnumerator FadeInCoroutinePernament(TextMeshPro text)
        {
            float waitTime = 0;
            float glowPowerTime = 0.20f;
            while (waitTime < 1)
            {
                text.enabled = true;
                text.fontMaterial.SetColor("_FaceColor", Color.Lerp(Color.clear, Color.white, waitTime));
                text.fontMaterial.SetColor("_OutlineColor", Color.Lerp(Color.clear, Color.gray, waitTime));
                if (waitTime < glowPowerTime)
                {
                    text.fontSharedMaterial.SetFloat("_GlowPower", waitTime);
                }
                yield return null;
                waitTime += Time.deltaTime / fadeSpeed;
            }
        }

        IEnumerator FadeOutCoroutine(TextMeshPro text)
        {
            float waitTime = 1;
            while (waitTime > 0)
            {
                text.fontMaterial.SetColor("_FaceColor", Color.Lerp(Color.clear, Color.white, waitTime));
                text.fontSharedMaterial.SetFloat("_GlowPower", waitTime);
                yield return null;
                waitTime -= Time.deltaTime / fadeSpeed;
                text.enabled = false;
            }
        }
    }
}
