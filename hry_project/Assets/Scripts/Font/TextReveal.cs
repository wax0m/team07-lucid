﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Debug = UnityEngine.Debug;
using TMPro;
using System.Diagnostics.Contracts;


namespace lucidGame.TextReveal
{
    public class TextReveal : MonoBehaviour
    {
        [SerializeField] private GameObject lightCollider;
        [SerializeField] private GameObject lightReveal;
        [SerializeField] private TextMeshPro m_TextA, m_TextB, m_TextC, m_TextD, m_TextE;

        private float m_isNearA, m_isNearB, m_isNearC, m_isNearD, m_isNearE;
        private float maxDist = 4.0f;

        public float fadeSpeed = 5.0f;
        private bool showHint;

        void Awake()
        {
            this.showHint = false;
            if (m_TextB == null)
            {
                m_TextB = null;
                m_TextC = null;
                m_TextD = null;
                m_TextE = null;
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            m_TextA.enabled = false;
            m_TextB.enabled = false;
            m_TextC.enabled = false;
            m_TextD.enabled = false;
            m_TextE.enabled = false;

            lightCollider = GameObject.FindGameObjectWithTag("Light");
            lightReveal = GameObject.FindGameObjectWithTag("Player");
        }

        // Update is called once per frame
        void Update()
        {

            checkShowHint();
            checkIsNear();
            if (showHint)
            {
                if (m_isNearA < maxDist)
                {
                    StartCoroutine(FadeInCoroutine(m_TextA));
                }
                else
                {
                    m_TextA.enabled = false;
                }
                
                if (m_isNearB < maxDist)
                {
                    StartCoroutine(FadeInCoroutine(m_TextB));
                }
                else
                {
                    m_TextB.enabled = false;
                }
                if (m_isNearC < maxDist)
                {
                    StartCoroutine(FadeInCoroutine(m_TextC));
                }
                else
                {
                    m_TextC.enabled = false;
                }
                if (m_isNearD < maxDist)
                {
                    StartCoroutine(FadeInCoroutine(m_TextD));
                }
                else
                {
                    m_TextD.enabled = false;
                }
                if (m_isNearE <= maxDist)
                {
                    StartCoroutine(FadeInCoroutine(m_TextE));
                }
                else
                {
                    m_TextE.enabled = false;
                }
            }
            else
            {
                m_TextA.enabled = false;
                m_TextB.enabled = false;
                m_TextC.enabled = false;
                m_TextD.enabled = false;
                m_TextE.enabled = false;
            }
        }

        private void checkShowHint()
        {
            if (lightCollider.GetComponent<LightReveal>().m_IsNearby && lightReveal.GetComponent<PlayerController>().isLightOn)
            {
                showHint = true;
            }
            else
            {
                showHint = false;
            }
        }

       private void checkIsNear()
       {
            m_isNearA = Vector3.Distance(m_TextA.transform.position, lightCollider.transform.position);
            m_isNearB = Vector3.Distance(m_TextB.transform.position, lightCollider.transform.position);
            m_isNearC = Vector3.Distance(m_TextC.transform.position, lightCollider.transform.position);
            m_isNearD = Vector3.Distance(m_TextD.transform.position, lightCollider.transform.position);
            m_isNearE = Vector3.Distance(m_TextE.transform.position, lightCollider.transform.position);
       }

        IEnumerator FadeInCoroutine(TextMeshPro text)
        {
            float waitTime = 0;
            float fadeTime = 1;
            while (waitTime < 1)
            {
                text.enabled = true;
                text.fontMaterial.SetColor("_FaceColor", Color.Lerp(Color.clear, Color.white, waitTime));
                text.fontMaterial.SetColor("_OutlineColor", Color.Lerp(Color.clear, Color.black, waitTime));
                text.fontSharedMaterial.SetFloat("_GlowPower", waitTime);
                yield return null;
                waitTime += Time.deltaTime / fadeSpeed;

               if (!showHint)
               {
                    text.enabled = false;
                    yield break;
               }
            }
        }

        IEnumerator FadeOutCoroutine(TextMeshPro text)
        {
            float waitTime = 1;
            while (waitTime > 0)
            {
                text.fontMaterial.SetColor("_FaceColor", Color.Lerp(Color.clear, Color.white, waitTime));
                text.fontSharedMaterial.SetFloat("_GlowPower", waitTime);
                yield return null;
                waitTime -= Time.deltaTime / fadeSpeed;
                m_TextA.enabled = false;
            }
        }
    }
}
