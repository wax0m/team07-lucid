﻿using UnityEngine;

namespace lucidGame
{
    public class ShowText : MonoBehaviour
    {
        public GameObject uiObj;
        // Start is called before the first frame update
        void Start()
        {
            uiObj.SetActive(false);
        }

        void OnTriggerEnter2D(Collider2D player){
            Debug.Log("Entered");
            if(player.gameObject.tag == "Player"){
                uiObj.SetActive(true);
            }
        }
    }
}
