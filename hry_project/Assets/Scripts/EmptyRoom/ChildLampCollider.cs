﻿using System;
using UnityEngine;

namespace lucidGame
{
    public class ChildLampCollider : MonoBehaviour
    {
        [SerializeField] private ShootTheLamp lampController;

        private void OnMouseDown()
        {
            lampController.ParentOnMouseDown();
        }
    }
}
