﻿using System;
using lucidGame.Control.Inventory;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace lucidGame
{
    public class KillYourSelf : MonoBehaviour
    {
        [SerializeField] private Item requiredInventoryItem;
        [SerializeField] private bool hasItem;
        [SerializeField] private GameObject endTimeline;
        [SerializeField] private GameObject executionCollider;

        public bool isPlayerInArea;

        private Collider2D trigger;

        private Inventory m_Inventory;

        private DragDropItem currentItem;
        
        
        // Start is called before the first frame update
        void Start()
        {
            m_Inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
            if (requiredInventoryItem == null) hasItem = true;
            else hasItem = false;

            trigger = GetComponent<Collider2D>();
            trigger.enabled = false;
            endTimeline.SetActive(false);
            isPlayerInArea = false;
            executionCollider.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            hasItem = CheckInventoryForItem();
            if (hasItem)
            {
                trigger.enabled = true;
                executionCollider.SetActive(true);
            }

            else
            {
                trigger.enabled = false;
                executionCollider.SetActive(false);
            }

            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log("Myska v update");
            }
        }

        private void OnMouseDown()
        {
            Debug.Log("Hej");
            if (hasItem && isPlayerInArea)
            {
                Destroy(currentItem.gameObject);
                endTimeline.SetActive(true);
                Invoke("KillPlayer", 9.0f);
            }
        }
        
        

        private void KillPlayer()
        {
            SceneManager.LoadScene("TheEnd");
        }

        private bool CheckInventoryForItem()
        {
            bool m_Return = false;
            if (requiredInventoryItem != null)
            {
                //Debug.Log("You need an item: " + requiredInventoryItem);
                //Debug.Log(m_Inventory.slots.Length);
                for (int i = 0; i < m_Inventory.slots.Length; i++)
                {
                    if (m_Inventory.slots[i].transform.childCount != 0)
                    {
                        currentItem = m_Inventory.slots[i].transform.GetChild(0).gameObject
                            .GetComponent<DragDropItem>();
                        //Debug.Log(m_Inventory.slots[i].transform.childCount);
                        //Debug.Log(m_Inventory.slots[i].transform.GetChild(0).gameObject.GetComponent<DragDropItem>());
                        if (currentItem.thisItem == requiredInventoryItem)
                        {
                            //Destroy(currentItem.gameObject);
                            m_Inventory.isFull[i] = false;
                            //Debug.Log("Hej");
                            m_Return = true;
                            
                            break;
                        }
                    }
                }
            }
            else m_Return = true;
            return m_Return;
        }
    }
}
