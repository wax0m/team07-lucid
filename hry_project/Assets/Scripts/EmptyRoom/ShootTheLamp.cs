﻿using System;
using lucidGame.Control.Inventory;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.EventSystems;

namespace lucidGame
{
    public class ShootTheLamp : MonoBehaviour
    {
    
        [SerializeField] private Item requiredInventoryItem;
        [SerializeField] private bool hasItem;

        [SerializeField] private GameObject m_Player;


        [SerializeField] private GameObject lamp;
        [SerializeField] private GameObject key;
        [SerializeField] private GameObject keyBlockage;

        [SerializeField] private GameObject brokenBulb;

        [SerializeField] private GameObject lights;
        
        private Inventory m_Inventory;
        private DragDropItem currentItem;
        private Animator m_PlayerAnimator;
        private PlayerController m_PlayerController;

        private void Start()
        {
            if (requiredInventoryItem == null) hasItem = true;
            else hasItem = false;
            if (m_Player == null)
            {
                m_Player = GameObject.FindGameObjectWithTag("Player");
            }

            m_PlayerController = m_Player.GetComponent<PlayerController>();
            m_Inventory = m_Player.GetComponent<Inventory>();
            m_PlayerAnimator = m_Player.GetComponent<Animator>();
            lamp.SetActive(true);
            brokenBulb.SetActive(true);
            lights.SetActive(true);
            key.GetComponent<Collider2D>().enabled = false;
        }

        private void LateUpdate()
        {
            hasItem = CheckInventoryForItem();
        }

        
        private void OnMouseDown()
        {
            //Debug.Log("Clicked On Lamp");
            if (hasItem)
            {
                float direction = transform.position.x - m_Player.transform.position.x;
                m_PlayerController.Flip(direction);
                m_PlayerAnimator.Play("Player_Shoot");
                float duration = m_PlayerAnimator.GetCurrentAnimatorStateInfo(0).length;
                Destroy(currentItem.gameObject);
                
                Invoke("DestroyLamp", duration);
            }
        }
        

        // DO NOT USE THIS
        public void ParentOnMouseDown()
        {
            if (hasItem)
            {
                DestroyLamp();
            }
        }
        //
        
        private void DestroyLamp()
        {
            key.GetComponent<Collider2D>().enabled = true;
            key.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;  
            lamp.SetActive(false);
            brokenBulb.SetActive(true);
            lights.SetActive(false);
        }


        private bool CheckInventoryForItem()
        {
            bool m_Return = false;
            if (requiredInventoryItem != null)
            {
                //Debug.Log("You need an item: " + requiredInventoryItem);
                //Debug.Log(m_Inventory.slots.Length);
                for (int i = 0; i < m_Inventory.slots.Length; i++)
                {
                    if (m_Inventory.slots[i].transform.childCount != 0)
                    {
                        currentItem = m_Inventory.slots[i].transform.GetChild(0).gameObject
                            .GetComponent<DragDropItem>();
                        //Debug.Log(m_Inventory.slots[i].transform.childCount);
                        //Debug.Log(m_Inventory.slots[i].transform.GetChild(0).gameObject.GetComponent<DragDropItem>());
                        if (currentItem.thisItem == requiredInventoryItem)
                        {
                            //Destroy(currentItem.gameObject);
                            m_Inventory.isFull[i] = false;
                            //Debug.Log("Hej");
                            m_Return = true;
                            
                            break;
                        }
                    }
                }
            }
            else m_Return = true;
            return m_Return;
        }

    }
}
