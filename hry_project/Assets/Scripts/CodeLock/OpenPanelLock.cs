using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.EventSystems;
using Debug = UnityEngine.Debug;


namespace lucidGame.CodeLock
{
    public class OpenPanelLock : MonoBehaviour
    {
        
        [SerializeField] private GameObject m_CodeLock;
        [SerializeField] private CodeAnimation m_LockController;
        [SerializeField] private GameObject cageClosed;
        [SerializeField] private GameObject cageOpened;

        private Collider2D collider;

        private bool areCages;

        private GameObject m_Player;
        private Animator m_PlayerAnimator;
        
        private bool m_IsNearby;
        
        public bool isUnlocked;

        void Awake()
        {
            m_Player = GameObject.FindGameObjectWithTag("Player");
            m_PlayerAnimator = m_Player.GetComponent<Animator>();
            m_IsNearby = false;
        }

        // Start is called before the first frame update
        void Start()
        {
            collider = GetComponent<Collider2D>();
            collider.enabled = false;
            m_CodeLock.SetActive(false);
            if (cageClosed != null && cageOpened != null)
            {
                areCages = true;
                cageClosed.SetActive(true);
                cageOpened.SetActive(false);
            }
        }

        public void ClosePanel()
        {
            if (m_CodeLock != null)
            {
                isUnlocked = m_LockController.m_IsSafeOpened;

                m_CodeLock.SetActive(false);

                Invoke("StopInteracting", 0.2f);
                //Debug.Log("CloseLock");
            }
        }

        private void StopInteracting()
        {
            m_PlayerAnimator.SetBool("Interact", false);
            
            if (areCages && isUnlocked)
            {
                cageClosed.SetActive(false);
                cageOpened.SetActive(true);
                collider.enabled = false;
            }
        }

        public void OpenPanel()
        {
            if (m_CodeLock != null)
            {
                isUnlocked = m_LockController.m_IsSafeOpened;
                
                m_PlayerAnimator.Play("Player_Interaction");
                m_PlayerAnimator.SetBool("Interact", true);

                //float delay = 3f;
                float delay = m_PlayerAnimator.GetCurrentAnimatorStateInfo(0).length;
                
                Invoke("SetActiveLock", delay - 1.5f);

                //Debug.Log("OpenLock");
            }
        }

        private void SetActiveLock()
        {
            m_CodeLock.SetActive(true);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                //Debug.Log("Lock is nearby");
                m_IsNearby = true;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                //Debug.Log("Left lock's range");
                m_IsNearby = false;
            }
        }

        private void OnMouseDown()
        {
            if (m_IsNearby && !isUnlocked)
            {
                OpenPanel();
            }
        }
    }

}
