﻿using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

namespace lucidGame
{
    public class LightDimmer : MonoBehaviour
    {
        [SerializeField] private GameObject m_Player;

        [SerializeField] private bool dimNow;
        [SerializeField] private Transform startingPoint, endingPoint;

        [SerializeField] private float dimAmount;
        
        [SerializeField] private Light2D worldLight;
        [SerializeField] private float minWL, maxWL;
        [SerializeField] private float intensityWL;

        [SerializeField] private Light2D skyLight;
        [SerializeField] private float minSL, maxSL;
        [SerializeField] private float intensitySL;

        [SerializeField] private Light2D cloudLight;
        [SerializeField] private float minCL, maxCL;
        [SerializeField] private float intensityCL;

        
        void Start()
        {
            maxWL = worldLight.intensity;
            maxSL = skyLight.intensity;
            maxCL = cloudLight.intensity;
        }

        void Update()
        {
            if (isDimming)
            {
                dimAmount = Mathf.Abs((endingPoint.position.x - m_Player.transform.position.x) / (startingPoint.position.x - endingPoint.position.x));
                dimAmount = Mathf.Clamp(dimAmount, 0, 1);
                
                intensityWL = Mathf.Clamp(dimAmount * maxWL, minWL, maxWL);
                worldLight.intensity = intensityWL;

                intensitySL = Mathf.Clamp(dimAmount * maxSL, minSL, maxSL);
                skyLight.intensity = intensitySL;

                intensityCL = Mathf.Clamp(dimAmount * maxCL, minCL, maxCL);
                cloudLight.intensity = intensityCL;
                // worldLight.intensity = Mathf.Clamp(startingPoint.position.x -);
            }
        }


        public bool isDimming
        {
            get { return dimNow; }
            set { dimNow = value; }
        }

        public Transform StartPoint
        {
            set { startingPoint = value; }
        }
    }
}
