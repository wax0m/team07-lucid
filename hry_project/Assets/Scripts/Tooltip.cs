﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Tooltip : MonoBehaviour
{
    private bool showToolTip;
    [SerializeField] private GameObject toolTip;

    private void Start()
    {
        showToolTip = false;
    }

    void Update()
    {
        if (showToolTip)
        {
            ShowToolTip();
        }
        else
        {
            HideToolTip();
        }
    }
    
    private void ShowToolTip()
    {
        toolTip.SetActive(true);
    }

    // Update is called once per frame
    private void HideToolTip()
    {
        toolTip.SetActive(false);
    }
    
    public void OnPointerEnter(PointerEventData eventData)
    { 
        showToolTip = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        showToolTip = false;
    }
}
