﻿using UnityEngine;

namespace lucidGame
{
    public class TimelinePlayerDeactivation : MonoBehaviour
    {
        [SerializeField] private GameObject player;


        private void Start()
        {
            Color tmp = player.GetComponent<SpriteRenderer>().color;
            tmp.a = 0f;
            player.GetComponent<SpriteRenderer>().color = tmp;
            //player.GetComponent<PlayerController>().isFrozen = true;
        }
        
        
        public void ActivateRealPlayer()
        {
            this.gameObject.SetActive(false);
            Color tmp = player.GetComponent<SpriteRenderer>().color;
            tmp.a = 255f;
            player.GetComponent<SpriteRenderer>().color = tmp;
            //player.GetComponent<PlayerController>().isFrozen = false;
        }
    }
}
