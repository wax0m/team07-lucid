﻿using System;
using UnityEngine;

namespace lucidGame
{
    public class LightReveal : MonoBehaviour
    {

        public bool m_IsNearby;


        private PlayerController m_PlayerController;

        [SerializeField] private bool isLightOn;
        [SerializeField] private GameObject lightCollider;

        private void Start()
        {
            m_PlayerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        }
        
        private void LateUpdate()
        {
            isLightOn = m_PlayerController.isLightOn;
            if (isLightOn)
            {
                lightCollider.SetActive(true);
            }
            else
            {
                lightCollider.SetActive(false);
            }
        }


    }
}
