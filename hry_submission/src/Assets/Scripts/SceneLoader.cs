﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace lucidGame
{
    public class SceneLoader : MonoBehaviour
    {

        public Animator m_Transition;
        public float transitionTime = 1f;
        public void LoadNextScene()
        {
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            StartCoroutine(LoadScene(SceneManager.GetActiveScene().buildIndex + 1));
        }

        IEnumerator LoadScene(int levelIndex)
        {
            m_Transition.SetTrigger("Start");
            yield return new WaitForSeconds(transitionTime);
            SceneManager.LoadScene(levelIndex);
        }
        
        public void ReturnToMainMenu()
        {
            SceneManager.LoadScene(0);
        }
        
    }
}
