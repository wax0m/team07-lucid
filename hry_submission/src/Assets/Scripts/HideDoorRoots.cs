﻿using System;
using System.Text;
using lucidGame.Control.Inventory;
using UnityEngine;

namespace lucidGame
{
    public class HideDoorRoots : MonoBehaviour
    {
        
        [SerializeField] private Item m_RequiredItem;
        [SerializeField] private bool m_ItemOwned;
        [SerializeField] private bool playerEnter;
        
        
        private Animator m_RootsAnimator;
        private Inventory m_PlayerInventory;

        // Start is called before the first frame update
        void Start()
        {
            m_PlayerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
            m_RootsAnimator = GetComponent<Animator>();
            playerEnter = false;
        }


        private void LateUpdate()
        {
            
            if (playerEnter && m_ItemOwned)
            {
                // Play fade out animation
                m_RootsAnimator.Play("Door_Roots_Fadeout");
                float animLength = m_RootsAnimator.GetCurrentAnimatorStateInfo(0).length;
                Invoke("SelfDisable", animLength);
            }
        }

        private void SelfDisable()
        {
            this.gameObject.SetActive(false);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                playerEnter = true;
                m_ItemOwned = CheckInventoryForItem();
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                playerEnter = false;
                m_ItemOwned = CheckInventoryForItem();
            }
        }

        private bool CheckInventoryForItem()
        {
            bool m_Return = false;
            if (m_RequiredItem != null)
            {
                for (int i = 0; i < m_PlayerInventory.slots.Length; i++)
                {
                    if (m_PlayerInventory.slots[i].transform.childCount != 0)
                    {
                        DragDropItem currentItem = m_PlayerInventory.slots[i].transform.GetChild(0).gameObject
                            .GetComponent<DragDropItem>();
                        if (currentItem.thisItem == m_RequiredItem)
                        {
                            m_PlayerInventory.isFull[i] = false;
                            m_Return = true;
                            break;
                        }
                    }
                }
            }
            else m_Return = true;
            return m_Return;
        }
    }
}
