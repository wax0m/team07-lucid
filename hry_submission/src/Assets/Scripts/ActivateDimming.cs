﻿using System;
using lucidGame.Control.Inventory;
using UnityEngine;

namespace lucidGame
{
    public class ActivateDimming : MonoBehaviour
    {
        [SerializeField] private LightDimmer m_LightDimmer;
        private Inventory m_Inventory;
        [SerializeField] private Item requiredInventoryItem;
        private bool waitForDimming;
        private void Start()
        {
            m_Inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
            waitForDimming = true;
        }

        private void LateUpdate()
        {
            if (CheckInventoryForItem() && waitForDimming)
            {
                waitForDimming = false;
                m_LightDimmer.isDimming = true;
            }
        }


        private bool CheckInventoryForItem()
        {
            bool m_Return = false;
            if (requiredInventoryItem != null)
            {
                //Debug.Log("You need an item: " + requiredInventoryItem);
                //Debug.Log(m_Inventory.slots.Length);
                for (int i = 0; i < m_Inventory.slots.Length; i++)
                {
                    if (m_Inventory.slots[i].transform.childCount != 0)
                    {
                        DragDropItem currentItem = m_Inventory.slots[i].transform.GetChild(0).gameObject
                            .GetComponent<DragDropItem>();
                        //Debug.Log(m_Inventory.slots[i].transform.childCount);
                        //Debug.Log(m_Inventory.slots[i].transform.GetChild(0).gameObject.GetComponent<DragDropItem>());
                        if (currentItem.thisItem == requiredInventoryItem)
                        {
                            //Destroy(currentItem.gameObject);
                            m_Inventory.isFull[i] = false;
                            //Debug.Log("Hej");
                            m_Return = true;
                            break;
                        }
                    }
                }
            }
            else m_Return = true;
            return m_Return;
        }
    }
}
