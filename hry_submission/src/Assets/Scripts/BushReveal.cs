﻿using System;
using lucidGame.CodeLock;
using lucidGame.Control.Inventory;
using UnityEngine;

namespace lucidGame
{
    public class BushReveal : MonoBehaviour
    {
        [SerializeField] private Item requiredInventoryItem;
    
        [SerializeField] private GameObject leftBush;
        [SerializeField] private GameObject rightBush;

        [SerializeField] private GameObject cage;
        private Collider2D cageCollider;
        [SerializeField] private OpenPanelLock m_LockOpener;

        private Collider2D thisBushCollider;
    
        private GameObject m_Player;
        private Animator m_PlayerAnimator;
        private Animator m_BushAminator;
        private Inventory m_Inventory;

        private Transform m_HeadCheck;
        private Transform m_KneeCheck;

        public bool hasItem;
        public bool isPlayerNearby;
        public bool isRevealed;
        public bool isUnlocked;
    
        private void Start()
        {
            m_Player = GameObject.FindGameObjectWithTag("Player");
            m_HeadCheck = m_Player.GetComponent<PlayerController>().headLevel;
            m_KneeCheck = m_Player.GetComponent<PlayerController>().kneeLevel;
            m_PlayerAnimator = m_Player.GetComponent<Animator>();
            m_BushAminator = GetComponent<Animator>();
            m_BushAminator.SetBool("Shaking", true);
            m_Inventory = m_Player.GetComponent<Inventory>();
            cageCollider = cage.GetComponent<Collider2D>();
            cageCollider.enabled = false;
            thisBushCollider = GetComponent<Collider2D>();
            thisBushCollider.enabled = true;

            leftBush.SetActive(true);
            rightBush.SetActive(true);
        }

        private void LateUpdate()
        {
            isUnlocked = m_LockOpener.isUnlocked;
            RevealBushes();

            if (isUnlocked)
            {
                cageCollider.enabled = false;
            }
        }

        private void RevealBushes()
        {
        
            // LEFT BUSH
            if (isRevealed)
            {
                cage.SetActive(true);
                m_BushAminator.SetBool("Shaking", false);
                leftBush.GetComponent<Animator>().Play("BushReveal");
                float length = leftBush.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length;
                Invoke("DeactivateLeftBush", length);
            }
        
            //RIGH BUSH
            if (isUnlocked)
            {
                rightBush.GetComponent<Animator>().Play("BushReveal");
                float length = rightBush.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length;
                Invoke("DeactivateRightBush", length);
            }
        }
    
        private void DeactivateLeftBush()
        {
            leftBush.SetActive(false);
            thisBushCollider.enabled = false;
            if (!isUnlocked) cageCollider.enabled = true;
        }

        private void DeactivateRightBush()
        {
            rightBush.SetActive(false);
        }
        /*
        public void ParentOnMouseDown()
        {
            CheckForBerries();
        }

        public void ParentOnTriggerEnter2D()
        {
            isPlayerNearby = true;
        }

        public void ParentOnTriggerExit2D()
        {
            isPlayerNearby = false;
        }

        */
        private void OnMouseDown()
        {
            CheckForBerries();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            isPlayerNearby = true;
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            isPlayerNearby = false;
        }


        public void CheckForBerries()
        {
            if (isPlayerNearby && CheckInventoryForItem())
            {
                isRevealed = true;
                InteractAnimation();
            }
            else isRevealed = false;
        }
    
        private bool CheckInventoryForItem()
        {
            bool ret = false;
            if (requiredInventoryItem != null)
            {
                for (int i = 0; i < m_Inventory.slots.Length; i++)
                {
                    if (m_Inventory.slots[i].transform.childCount != 0)
                    {
                        DragDropItem currentItem = m_Inventory.slots[i].transform.GetChild(0).gameObject
                            .GetComponent<DragDropItem>();
                        if (currentItem.thisItem == requiredInventoryItem)
                        {
                            GameObject.Destroy(currentItem.gameObject);
                            m_Inventory.isFull[i] = false;
                            //Debug.Log("Hej");
                            ret = true;
                            hasItem = true;
                            break;
                        }
                    }
                }
            }
            else
            {
                ret = true;
                hasItem = true;

            }
            return ret;
        }

        private void InteractAnimation()
        {
            float itemY = transform.position.y;
            float playerHead = m_HeadCheck.position.y;
            float playerKnees = m_KneeCheck.position.y;
            if (itemY < playerHead && itemY > playerKnees)
            {
                m_PlayerAnimator.Play("Player_PickUpMid");
            }
            else if (itemY > playerHead)
            {
                m_PlayerAnimator.Play("Player_PickUpTop");
            }
            else if (itemY < playerKnees)
            {
                m_PlayerAnimator.Play("Player_PickUpBot");
            }
        }
    }
}
