﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace lucidGame
{
    public class PauseMenu : MonoBehaviour
    {
        public static bool isPaused = false;
        public GameObject pauseMenuUI;

        // Update is called once per frame
        void Update()
        {
            if(Input.GetKeyDown(KeyCode.Escape)){
                if(isPaused){
                    Resume();
                }else
                {
                    Pause();
                }
            }
        }

        public void Resume(){
            pauseMenuUI.SetActive(false);
            //InventoryPopup.showInventory = true;
            Time.timeScale = 1f;
            isPaused = false;
        }

        void Pause(){
            pauseMenuUI.SetActive(true);
            //InventoryPopup.showInventory = true;
            Time.timeScale = 0f;
            isPaused = true;
        }

        public void LoadMenu(){
            Resume();
            Debug.Log("loading menu");
            SceneManager.LoadScene(0);
        }

        public void QuitGame(){
            Debug.Log("Quiting");
            Application.Quit();
        }
        public void ExitGame(){
            Debug.Log("Exit");
            Application.Quit();
        }
    }
}
