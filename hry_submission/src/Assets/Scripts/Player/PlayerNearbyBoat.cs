﻿using System;
using Cinemachine;
using TMPro;
using UnityEditor.UIElements;
using UnityEngine;

namespace lucidGame
{
    public class PlayerNearbyBoat : MonoBehaviour
    {

        [SerializeField]
        private bool isPlayerNearby = false;


        public bool IsPlayerNearby()
        {
            return isPlayerNearby;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                isPlayerNearby = true;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                isPlayerNearby = false;
            }
        }
    }
}
