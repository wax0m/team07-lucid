﻿using System;
using System.Collections;
using lucidGame.CodeLock;
using TMPro;
using UnityEngine;

namespace lucidGame
{
    public class PetController : MonoBehaviour
    {
        [SerializeField] public float moveSpeed; 
        
        [SerializeField] private bool canRepairGenerator;

        [SerializeField] private float minimalRangeDistance;
        [SerializeField] private float maximalRangeDistance;

        [SerializeField] private float distanceFromPlayer;

        [SerializeField] private Collider2D petCollider;
        //[SerializeField] private GameObject childCollider;
        
        private GameObject m_Player;
        private PlayerController m_PlayerController;
        private Animator m_PetAnimator;
        private Vector3 m_TargetPosition;

        private Rigidbody2D rb;

        
        
        public float m_Direction;
        public bool isFacingRight;
        [SerializeField] private bool isGhosting;
        public bool isFollowing;
        public bool isFrozen;
        public bool isIdling;
        public bool isRunning;
        public bool isJumping;
        public bool isPlayerNearby;
        public bool isGettingFree;

        
        // Start is called before the first frame update
        void Start()
        {
            m_Player = GameObject.FindGameObjectWithTag("Player");
            m_PlayerController = m_Player.GetComponent<PlayerController>();
            rb = GetComponent<Rigidbody2D>();
            m_PetAnimator = GetComponent<Animator>();
            //childCollider.SetActive(false);
            
            isIdling = true;
        }

        // Update is called once per frame
        private void Update()
        {
            distanceFromPlayer = Vector2.Distance(transform.position, m_Player.transform.position);
            m_Direction = Mathf.Clamp(m_Player.transform.position.x - transform.position.x, -1, 1);
            
            if (transform.localScale.x < 0)
            {
                isFacingRight = false;
            }
            else
            {
                isFacingRight = true;
            }
            if (distanceFromPlayer <= maximalRangeDistance)
            {
                isPlayerNearby = true;
            }
            else
            {
                isPlayerNearby = false;
            }

            if (distanceFromPlayer <= minimalRangeDistance/* || distanceFromPlayer >= maximalRangeDistance*/)
            {
                GhostObject();
            }
            else
            {
                UnGhostObject();
            }

            if (isFrozen)
            {
                GhostObject();
            }

            FollowPlayer();
            AnimatePet();
            
            Flip();


        }
        
        void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(this.transform.position, minimalRangeDistance);
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(this.transform.position, maximalRangeDistance);
        }

        
        private void FollowPlayer()
        {
            if (isFollowing && !isFrozen)
            {
                if (distanceFromPlayer > maximalRangeDistance && distanceFromPlayer > minimalRangeDistance)
                {
                    //transform.position = Vector3.MoveTowards(transform.position, m_TargetPosition, moveSpeed * Time.deltaTime);
                    rb.velocity = new Vector2(m_Direction * moveSpeed, rb.velocity.y);
                    isRunning = true;
                    isIdling = false;
                }
                else
                {
                    isIdling = true;
                    isRunning = false;
                }
            }
        }

        private void Flip()
        {
            if (!isFrozen)
            {
                if (isRunning && !isGettingFree && !isJumping)
                {
                    if (m_Direction < 0 && isFacingRight)
                    {
                        Vector3 flippedScale = transform.localScale;
                        flippedScale.x *= -1;
                        transform.localScale = flippedScale;
                    }
                    else if (m_Direction > 0 && !isFacingRight)
                    {
                        Vector3 flippedScale = transform.localScale;
                        flippedScale.x *= -1;
                        transform.localScale = flippedScale;
                    }
                }
            }
            
        }

        public void GhostObject()
        {
            //rb.gravityScale = 0;
            rb.bodyType = RigidbodyType2D.Kinematic;
            petCollider.isTrigger = true;
        }

        public void UnGhostObject()
        {
            //rb.gravityScale = m_DefaultGravity;
            rb.bodyType = RigidbodyType2D.Dynamic;
            petCollider.isTrigger = false;
        }

        private void AnimatePet()
        {
            //if (isIdling)
            //{
                m_PetAnimator.SetBool("Idling", isIdling);
                //m_PetAnimator.SetBool("Running", false);
            //}
            
            //else if (!isIdling)
            //{
                m_PetAnimator.SetBool("Running", isRunning);
            //}
            if (isJumping)
            {
                m_PetAnimator.SetBool("Jumping", true);
            }
            else
            {
                m_PetAnimator.SetBool("Jumping", false);
            }
        }

    }
}
