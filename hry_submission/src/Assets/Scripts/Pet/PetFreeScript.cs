﻿using System;
using lucidGame.CodeLock;
using lucidGame.Control.Inventory;
using UnityEngine;

namespace lucidGame
{
    public class PetFreeScript : MonoBehaviour
    {
        [SerializeField] private Item requiredInventoryItem;
        [SerializeField] private bool hasItem;
        [SerializeField] private bool waitingToGetfree;
        [SerializeField] private OpenPanelLock m_CageOpener;
        [SerializeField] private bool isCageOpen;
        [SerializeField] private Collider2D keyCollider;
        [SerializeField] private bool hidingKey;
        [SerializeField] private bool isPlayerNearby;
        
        private GameObject m_Player;
        private Animator m_PlayerAnimator;
        private PlayerController m_PlayerController;
        private PetController m_PetController;
        private Animator m_PetAnimator;
        private Vector3 m_TargetPosition;
        
        private Inventory m_Inventory;

        private void Start()
        {
            m_Player = GameObject.FindGameObjectWithTag("Player");
            m_PlayerController = m_Player.GetComponent<PlayerController>();
            m_Inventory = m_Player.GetComponent<Inventory>();
            m_PlayerAnimator = m_Player.GetComponent<Animator>();
            m_PetController = GetComponent<PetController>();
            m_PetAnimator = GetComponent<Animator>();
            
            
            isCageOpen = false;
            waitingToGetfree = true;
            
            keyCollider.enabled = false;
        }

        private void LateUpdate()
        {
            isPlayerNearby = m_PetController.isPlayerNearby;
            isCageOpen = m_CageOpener.isUnlocked;

            if (waitingToGetfree && isCageOpen)
            {
                if (m_PetController.m_Direction < 0 && m_PetController.isFacingRight)
                {
                    Vector3 flippedScale = transform.localScale;
                    flippedScale.x *= -1;
                    transform.localScale = flippedScale;
                }
                else if (m_PetController.m_Direction > 0 && !m_PetController.isFacingRight)
                {
                    Vector3 flippedScale = transform.localScale;
                    flippedScale.x *= -1;
                    transform.localScale = flippedScale;
                }
            }
        }

        private void OnMouseDown()
        {

            Debug.Log("Clicked on pet");
            if (waitingToGetfree && isCageOpen && isPlayerNearby && CheckInventoryForItem())
            {
                waitingToGetfree = false;
                hidingKey = false;
                keyCollider.enabled = true;
                float offset = 3.0f;
                if (m_PlayerController.m_FacingRight)
                {
                    offset *= -1;
                }
                m_TargetPosition = new Vector3(m_Player.transform.position.x + offset, transform.position.y, transform.position.z);
                
                // FLIP
                if (m_PetController.m_Direction < 0 && m_PetController.isFacingRight)
                {
                    Vector3 flippedScale = transform.localScale;
                    flippedScale.x *= -1;
                    transform.localScale = flippedScale;
                }
                else if (m_PetController.m_Direction > 0 && !m_PetController.isFacingRight)
                {
                    Vector3 flippedScale = transform.localScale;
                    flippedScale.x *= -1;
                    transform.localScale = flippedScale;
                }
                
                
                
                // Animate pet eating berries and player interacting then start following
                m_PlayerAnimator.Play("Player_Interaction");
                m_PlayerAnimator.SetBool("Interact", true);
                m_PetAnimator.Play("Pet_EatBerries");
                float eatingTime = m_PetAnimator.GetCurrentAnimatorStateInfo(0).length;

                Invoke("StopEating", eatingTime - 0.5f);
            }
            
        }
        
        void FixedUpdate()
        {
            if (m_PetController.isGettingFree)
            {
                //m_PetAnimator.Play("Pet_Eat");
                GetToPlayer();
            }
        }

        private void StopEating()
        {
            m_PlayerAnimator.SetBool("Interact", false);
            m_PetController.isGettingFree = true;
        }
        
        private void GetToPlayer()
        {
            transform.position = Vector3.MoveTowards(transform.position, m_TargetPosition, m_PetController.moveSpeed * Time.deltaTime);
            //m_PetAnimator.SetBool("Idling", m_PetController.isIdling);
            //m_PetAnimator.SetBool("Running", m_PetController.isRunning);
            //rb.velocity = new Vector2();
            //GhostObject();
            
            m_PetController.isIdling = false;
            m_PetController.isRunning = true;
            
            if (Mathf.Abs(transform.position.x - m_TargetPosition.x) < 0.3f)
            {
                m_PetController.isRunning = false;
                m_PetController.isGettingFree = false;
                m_PetController.isIdling = true;
                m_PetController.isFollowing = true;
                //UnGhostObject();
            }
        }
        
        private bool CheckInventoryForItem()
        {
            bool ret = false;
            if (requiredInventoryItem != null)
            {
                for (int i = 0; i < m_Inventory.slots.Length; i++)
                {
                    if (m_Inventory.slots[i].transform.childCount != 0)
                    {
                        DragDropItem currentItem = m_Inventory.slots[i].transform.GetChild(0).gameObject
                            .GetComponent<DragDropItem>();
                        if (currentItem.thisItem == requiredInventoryItem)
                        {
                            Destroy(currentItem.gameObject);
                            m_Inventory.isFull[i] = false;
                            ret = true;
                            hasItem = true;
                            break;
                        }
                    }
                }
            }
            else
            {
                ret = true;
                hasItem = true;

            }
            return ret;
        }


    }
}
