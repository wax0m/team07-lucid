﻿using System;
using System.Collections;
using System.Numerics;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace lucidGame
{
    public class MovePetAway : MonoBehaviour
    {

        [SerializeField] private Transform endPoint;
        [SerializeField] private GameObject pet;
        [SerializeField] private float duration;
        [SerializeField] private bool transportPet;

        private Vector2 position;
        private PetController m_PetController;
        private bool triggered;
        private Collider2D collider;
        private Vector2 end;


        private void Start()
        {
            position = endPoint.position;
            m_PetController = pet.GetComponent<PetController>();
            triggered = false;
            collider = GetComponent<Collider2D>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                if (other.GetComponent<PlayerController>().isDragging)
                {
                    triggered = true;
                    m_PetController.isIdling = false;
                    m_PetController.isFrozen = true;
                    m_PetController.isRunning = true;
                    pet.GetComponent<WaitingToGetIntoShaft>().WaitingForShaft = true;
                    Flip();
                    end = new Vector2(position.x, pet.transform.position.y);
                    //transportPet = true;
                    
                    //AnimatePet();
                    //Vector2 end = new Vector2(position.x, pet.transform.position.y);
                    StartCoroutine(MovePet(end, duration));
                    pet.GetComponent<WaitingToGetIntoShaft>().WaitingForShaft = true;
                    
                    if (triggered)
                    {
                        collider.enabled = false;
                    }
                }
            }
            else if (other.CompareTag("Pushable"))
            {
                triggered = true;
                m_PetController.isIdling = false;
                m_PetController.isFrozen = true;
                pet.GetComponent<WaitingToGetIntoShaft>().WaitingForShaft = true;
                m_PetController.isRunning = true;
                Flip();
                end = new Vector2(position.x, pet.transform.position.y);
                //transportPet = true;
                
                //AnimatePet();
                //Vector2 end = new Vector2(position.x, pet.transform.position.y);
                StartCoroutine(MovePet(end, duration));
                pet.GetComponent<WaitingToGetIntoShaft>().WaitingForShaft = true;
                
                if (triggered)
                {
                    collider.enabled = false;
                }
            }
        }
        /*
        private void FixedUpdate()
        {
            if (transportPet)
            {
                GetToPosition();
            }
        }

        private void GetToPosition()
        {
            pet.transform.position =
                Vector2.MoveTowards(pet.transform.position, end, 
                    m_PetController.moveSpeed * Time.deltaTime);
            m_PetController.isIdling = false;
            m_PetController.isRunning = true;

            if (Mathf.Abs(pet.transform.position.x - end.x) < 0.3f)
            {
                m_PetController.isRunning = false;
                m_PetController.isIdling = true;
            }
        }

        private void AnimatePet()
        {
            m_PetController.isIdling = false;
            m_PetController.isFollowing = true;
            m_PetController.isRunning = true;
            pet.GetComponent<Animator>().SetBool("Idling", false);
            pet.GetComponent<Animator>().SetBool("Running", true);
        }
        */

        private IEnumerator MovePet(Vector2 goalPosition, float durat)
        {
            float time = 0;

            //Flip();

            
            
            Vector2 startValue = pet.transform.position;
            while (time < durat)
            {
                pet.transform.position = Vector2.Lerp(startValue, goalPosition, time / duration);
                time += Time.deltaTime;
                yield return null;
            }
            
            m_PetController.isIdling = true;
            m_PetController.isFrozen = false;
            m_PetController.isRunning = false;
            m_PetController.isFollowing = false;

            if (triggered)
            {
                Destroy(this.gameObject);
            }
        }

        private void Flip()
        {
            Debug.Log("Flipped");
            float m_Direction = Mathf.Clamp(transform.position.x - pet.transform.position.x, -1, 1);
            // Mathf.Clamp(m_Player.transform.position.x - transform.position.x, -1, 1);
            // FLIP
            if (m_Direction < 0 && m_PetController.isFacingRight)
            {
                Vector3 flippedScale = pet.transform.localScale;
                flippedScale.x *= -1;
                pet.transform.localScale = flippedScale;
            }
            else if (m_Direction > 0 && !m_PetController.isFacingRight)
            {
                Vector3 flippedScale = pet.transform.localScale;
                flippedScale.x *= -1;
                pet.transform.localScale = flippedScale;
            }

        }
        
    }
}
