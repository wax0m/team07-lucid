﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace lucidGame.Control.Inventory
{
    public class DragDropItem : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        //[SerializeField] private Canvas canvas;
        //[SerializeField] private InventoryPopup m_InventoryPopup;
        private InventoryPopup m_InventoryPopup;
        public Item thisItem;
        public Transform parentToReturnTo;
        public bool inventoryRequirements = false;
        //private CraftableItem item;
        
        //public DragDropItem requiredItem;
        public Item requiredItem;
        public DragDropItem craftingItem;
        public int currentPhase;

        
        
        
        
        private void Awake()
        {
            //rectTransform = GetComponent<RectTransform>();
            //item = GetComponent<CraftableItem>();
            m_InventoryPopup = GameObject.FindGameObjectWithTag("Inventory").GetComponent<InventoryPopup>();
            if (requiredItem != null) inventoryRequirements = true;
            currentPhase = thisItem.phase;
        }
        
        
        public void OnBeginDrag(PointerEventData eventData)
        {
            //Debug.Log("You just picked: " + item.titleName);
            Debug.Log("You just picked: " + thisItem.name);
            
            parentToReturnTo = this.transform.parent;
            this.transform.SetParent(this.transform.parent.parent);

            GetComponent<CanvasGroup>().blocksRaycasts = false;
            GetComponent<CanvasGroup>().alpha = 0.5f;
        }

        public void OnDrag(PointerEventData eventData)
        {
            //Debug.Log("Drag");
            m_InventoryPopup.ShowInv = true;
            this.transform.position = eventData.position;
            //rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            Debug.Log("OnEndDrag");
            this.transform.SetParent(parentToReturnTo);
        
            GetComponent<CanvasGroup>().blocksRaycasts = true;
            GetComponent<CanvasGroup>().alpha = 1f;
            
            //EventSystem.current.RaycastAll(eventData);
        }
    
        public void OnPointerDown(PointerEventData eventData)
        {
            Debug.Log("OnPointerDown");
        }

        public void OnCollisionEnter(Collision other)
        {
        }
    }
}
