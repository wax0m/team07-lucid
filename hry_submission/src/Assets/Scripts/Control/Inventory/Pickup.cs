﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace lucidGame.Control.Inventory
{
    // ITEM PICKUP BY CLICKING ON IT
    public class Pickup : MonoBehaviour/*, IPointerClickHandler*/
    {
        //[SerializeField] private GameObject m_InventoryPopup;
        private InventoryPopup m_InventoryPopup;
        private GameObject m_Player;
        private Animator m_PlayerAnimator;
        private Inventory m_Inventory;
        private Transform m_HeadCheck;
        private Transform m_KneeCheck;

        //public Sprite sprite;

        [SerializeField] private bool m_IsNearby;
        private bool m_RequirementCheck;
        
        public DragDropItem itemButton;
        public Item requiredInventoryItem;
        // Start is called before the first frame update
        void Start()
        {
            m_Player = GameObject.FindGameObjectWithTag("Player");
            m_Inventory = m_Player.GetComponent<Inventory>();
            m_HeadCheck = m_Player.GetComponent<PlayerController>().headLevel;
            m_KneeCheck = m_Player.GetComponent<PlayerController>().kneeLevel;
            m_PlayerAnimator = m_Player.GetComponent<Animator>();
            //m_Inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
            m_InventoryPopup = GameObject.FindGameObjectWithTag("Inventory").GetComponent<InventoryPopup>();
        }

        void Awake()
        {
            m_IsNearby = false;
        }
        private void OnMouseDown()
        {
            Debug.Log("Clicked");
            CheckInventoryAndDestroyObject();
        }
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                //Debug.Log("Item is nearby");
                m_IsNearby = true;
            }
        }
        
        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                //Debug.Log("You left item range");
                m_IsNearby = false;
            }
            
        }
        
        private void CheckInventoryAndDestroyObject()
        {
            if (m_IsNearby && CheckInventoryForItem())
            {
                for (int i = 0; i < m_Inventory.slots.Length; i++)
                {
                    if (!m_Inventory.isFull[i])
                    {
                        m_Inventory.isFull[i] = true;
                        Instantiate(itemButton, m_Inventory.slots[i].transform, false);
                        Destroy(gameObject);
                        PickUpAnimation();
                        //m_InventoryPopup.ShowInv = true;
                        m_InventoryPopup.PopAndHide();
                        break;
                    }
                }
            }
        }

        private bool CheckInventoryForItem()
        {
            bool m_Return = false;
            if (requiredInventoryItem != null)
            {
                //Debug.Log("You need an item: " + requiredInventoryItem);
                //Debug.Log(m_Inventory.slots.Length);
                for (int i = 0; i < m_Inventory.slots.Length; i++)
                {
                    if (m_Inventory.slots[i].transform.childCount != 0)
                    {
                        DragDropItem currentItem = m_Inventory.slots[i].transform.GetChild(0).gameObject
                            .GetComponent<DragDropItem>();
                        //Debug.Log(m_Inventory.slots[i].transform.childCount);
                        //Debug.Log(m_Inventory.slots[i].transform.GetChild(0).gameObject.GetComponent<DragDropItem>());
                        if (currentItem.thisItem == requiredInventoryItem)
                        {
                            Destroy(currentItem.gameObject);
                            m_Inventory.isFull[i] = false;
                            //Debug.Log("Hej");
                            m_Return = true;
                            break;
                        }
                    }
                }
            }
            else m_Return = true;
            return m_Return;
        }


        private void PickUpAnimation()
        {
            float itemY = transform.position.y;
            float playerHead = m_HeadCheck.position.y;
            float playerKnees = m_KneeCheck.position.y;
            if (itemY < playerHead && itemY > playerKnees)
            {
                m_PlayerAnimator.Play("Player_PickUpMid");
            }
            else if (itemY > playerHead)
            {
                m_PlayerAnimator.Play("Player_PickUpTop");
            }
            else if (itemY < playerKnees)
            {
                m_PlayerAnimator.Play("Player_PickUpBot");
            }
        }
    }
}
