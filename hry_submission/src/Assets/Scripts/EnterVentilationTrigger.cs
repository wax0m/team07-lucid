﻿using System;
using System.Collections;
using UnityEngine;

namespace lucidGame
{
    public class EnterVentilationTrigger : MonoBehaviour
    {
        [SerializeField] private GameObject m_Player;
        [SerializeField] private GameObject pet;
        
        private PetController m_PetController;
        private WaitingToGetIntoShaft jumpInShaftController;
        
        [SerializeField] private bool isPlayerNearby;
        
        
        private void Start()
        {
            jumpInShaftController = pet.GetComponent<WaitingToGetIntoShaft>();
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                isPlayerNearby = true;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                isPlayerNearby = false;
            }
        }

        private void OnMouseDown()
        {
            if (isPlayerNearby)
            {
                jumpInShaftController.EnterShaft();
                GetComponent<Collider2D>().enabled = false;
            }
        }

        
    }
}
