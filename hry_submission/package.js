{
  "title": "Lucid",
  "team": 7,
  "authors": [
    {"fullname": "Hung Pham", "email": "phamhun@fel.cvut.cz"},
    {"fullname": "An Hoai Pham", "email": "phamhoai@fel.cvut.cz"}
    {"fullname": "Lucie Rosenthalerová", "email": "rosenluc@fel.cvut.cz"}
    {"fullname": "Vilém Jonák", "email": "jonakvil@fel.cvut.cz"}
  ],
  "term": 201,
  "summary": "Puzzle game, where you try to escape from a nightmare by solving riddles.",
  "opensource": false
}
