LUCID

made by Team 7.

Members: 
Hung Pham phamhun@fel.cvut.cz
An Hoai Pham phamhoai@fel.cvut.cz
Lucie Rosenthalerová rosenluc@fel.cvut.cz
Vilém Jonák jonakvil@fel.cvut.cz

Lucid is 2D puzzle point and click adventure game. The player must search for items and slove riddles in order to pass a level. 
The game has three levels in total. Lucid takes place in a nightmare and has a distinct fantasy setting.
